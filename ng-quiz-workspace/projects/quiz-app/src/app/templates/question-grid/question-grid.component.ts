import { Component, OnInit } from '@angular/core';
import { MatGridListModule } from '@angular/material/grid-list';

@Component({
  selector: 'app-question-grid',
  templateUrl: './question-grid.component.html',
  imports: [MatGridListModule],
  standalone: true,
})
export class QuestionGridComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
