import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  imports: [RouterLink],
  standalone: true,
})
export class WelcomePageComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
