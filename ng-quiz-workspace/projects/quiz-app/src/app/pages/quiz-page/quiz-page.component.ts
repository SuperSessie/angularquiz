import { Component, OnInit } from '@angular/core';
import { QuestionGridComponent } from '../../templates/question-grid/question-grid.component';
import { QuizService } from '../../../../../quiz-lib/src/services';
import { Question } from '../../../../../quiz-lib/src/models/question';
import { CommonModule } from '@angular/common';
import {
  AnswerComponent,
  QuestionComponent,
  ScoreComponent,
} from '../../../../../quiz-lib/src/components';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-quiz-page',
  templateUrl: './quiz-page.component.html',
  standalone: true,
  imports: [
    CommonModule,
    QuestionGridComponent,
    QuestionComponent,
    AnswerComponent,
    ScoreComponent,
    RouterLink,
  ],
})
export class QuizPageComponent implements OnInit {
  constructor(readonly service: QuizService) {}

  question: Question | null = null;

  right: number = 0;
  wrong: number = 0;

  ngOnInit() {
    this.nextQuestion();
  }

  answerClicked(index: number) {
    if (index - 1 == this.question?.correctAnswer) {
      this.right++;
      this.nextQuestion();
    } else {
      this.wrong++;
    }
  }

  nextQuestion() {
    this.question = this.service.GetRandomQuestion();
  }

  reset() {
    console.log('Reset de score');
    this.right = 0;
    this.wrong = 0;
  }
}
