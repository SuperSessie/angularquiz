import { Injectable } from '@angular/core';
import { Question } from '../models/question';

@Injectable({
  providedIn: 'root',
})
export class QuizService {
  questions: Question[];

  constructor() {
    this.questions = [];

    this.questions.push({
      content: 'Waarom werkt mijn Angular niet?',
      answerA: 'Internet problemen',
      answerB: 'Te weinig gedronken',
      answerC: 'Bijna weekend...',
      answerD: 'Eten 😸',
      correctAnswer: 0,
    } as Question);

    this.questions.push({
      content: 'Wat is de 3e macht van 24',
      answerA: 'Weet ik veel... 🤔',
      answerB: 'Hekkie!?',
      answerC: '13824',
      answerD: '12345',
      correctAnswer: 2,
    } as Question);
  }

  public GetRandomQuestion(): Question {
    const random = Math.floor(Math.random() * this.questions.length);
    return this.questions[random];
  }
}
