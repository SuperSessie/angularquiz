import { Component, Input, OnInit } from '@angular/core';
import { Question } from '../../models/question';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css'],
  standalone: true,
})
export class QuestionComponent implements OnInit {
  @Input({ required: true }) question!: Question;

  constructor() {}

  ngOnInit() {}
}
