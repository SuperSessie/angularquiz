import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css'],
  standalone: true,
})
export class AnswerComponent implements OnInit {
  @Input({ required: true }) answer!: string;
  @Input({ required: true }) index!: number;

  @Output() answerClicked = new EventEmitter<number>();

  constructor() {}

  ngOnInit() {}

  onClick() {
    console.log(`${this.answer} clicked!`);
    this.answerClicked.emit(this.index);
  }
}
