export { QuestionComponent } from './question/question.component';
export { AnswerComponent } from './answer/answer.component';
export { ScoreComponent } from './score/score.component';
