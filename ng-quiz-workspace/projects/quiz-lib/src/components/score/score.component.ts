import { Component, Input, OnInit, input } from '@angular/core';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.css'],
  standalone: true,
})
export class ScoreComponent implements OnInit {
  @Input() right: number;
  @Input() wrong: number;

  constructor() {
    this.right = 0;
    this.wrong = 0;
  }

  ngOnInit() {}
}
