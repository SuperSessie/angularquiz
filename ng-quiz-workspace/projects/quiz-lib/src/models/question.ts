/** Dit is een vraag met 4 antwoorden */
export class Question {
  public content: string;

  public answerA: string;
  public answerB: string;
  public answerC: string;
  public answerD: string;

  /** A = 0, B = 1, C = 2, D = 3 */
  public correctAnswer: number;

  constructor() {
    this.content = '';
    this.answerA = '';
    this.answerB = '';
    this.answerC = '';
    this.answerD = '';
    this.correctAnswer = 0;
  }
}
