# AngularQuiz

## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/SuperSessie/angularquiz.git
git branch -M main
git push -uf origin main
```

## Tools

- Visual Studio Code
- NodeJs
- Angular CLI

## Visual Studio Code Extensions

- Angular Files
- Angular Language Service
- Angular Schematics
- EsLint
- Prettier
